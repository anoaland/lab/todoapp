import { AppStyleProps } from '../../styles'

export interface SmallButtonProps extends Partial<AppStyleProps> {
  text: string
  onPress?: () => void
}
