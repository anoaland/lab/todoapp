import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { AppStyle } from '../../styles'
import { SmallButtonProps } from './props'
import { SmallButtonState } from './state'

@AppStyle.withThemeClass()
export class SmallButton extends React.Component<
  SmallButtonProps,
  SmallButtonState
> {
  constructor(props: SmallButtonProps) {
    super(props)
    this.state = {
      pressed: false
    }
  }

  public render() {
    const {
      text,
      onPress,
      theme: {
        styles,
        props: { colors, sizes }
      }
    } = this.props as Required<SmallButtonProps>

    // if (!theme) {
    //   throw new Error('Theme is not ready!')
    // }

    return (
      <TouchableOpacity onPress={onPress}>
        <View style={[{ padding: sizes.xs }, styles.darkButton]}>
          <Text style={{ color: colors.light }}>{text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
