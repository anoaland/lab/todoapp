import { LightTheme } from './light'

export const RedTheme = LightTheme.extend(
  {
    colors: {
      dark: '#d81b60'
    }
  },
  props => ({
    // override default theme styles
  })
)
