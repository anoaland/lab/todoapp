import { createTheme } from 'anoa-react-native-theme'

export const LightTheme = createTheme(
  {
    colors: {
      primary: '#bbdefb',
      light: '#eeffff',
      dark: '#8aacc8'
    },
    sizes: {
      xs: 10,
      sm: 20,
      lg: 30
    }
  },
  props => ({
    container: {
      flex: 1,
      padding: props.sizes.lg,
      backgroundColor: props.colors.primary
    },
    lightContainer: {
      flex: 1,
      backgroundColor: props.colors.light
    },
    button: {
      padding: 20
    },
    lightButton: {
      backgroundColor: props.colors.light
    },
    darkButton: {
      backgroundColor: props.colors.dark
    }
  })
)
