import React from 'react'
import { Text, TextInput, View } from 'react-native'
import { AppStore } from '../../../store'
import { setUserNameAction } from '../../../store/actions/set-user-name-action'
import { SmallButton } from '../../components/small-button'
import { AppStyle } from '../../styles'
import {
  HelpScreenActionProps,
  HelpScreenProps,
  HelpScreenStateProps
} from './props'
import { HelpScreenState } from './state'

@AppStore.withStoreClass<HelpScreenStateProps, HelpScreenActionProps>(
  state => ({ userName: state.user.name }),
  dispatch => ({ setUserName: payload => dispatch(setUserNameAction(payload)) })
)
@AppStyle.withThemeClass()
export class HelpScreen extends React.Component<
  HelpScreenProps,
  HelpScreenState
> {
  constructor(props: HelpScreenProps) {
    super(props)
    this.state = {
      name: props.userName as string
    }
  }

  public render() {
    const {
      setUserName,
      theme: { styles }
    } = this.props as Required<HelpScreenProps>
    const { name } = this.state
    return (
      <View style={styles.container}>
        <Text>{name}</Text>
        <TextInput
          value={name}
          onChangeText={text => {
            this.setState({ name: text })
          }}
        />
        <SmallButton
          text="Simpan"
          onPress={() => {
            setUserName(name)
          }}
        />
      </View>
    )
  }
}
