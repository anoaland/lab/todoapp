import { AppStyleProps } from '../../styles'

export interface HelpScreenProps
  extends Partial<HelpScreenStateProps>,
    Partial<HelpScreenActionProps>,
    Partial<AppStyleProps> {}

export interface HelpScreenStateProps {
  userName: string
}

export interface HelpScreenActionProps {
  setUserName: (payload: string) => void
}
