import { AppStyleProps } from '../../styles'

export interface AboutScreenProps
  extends Partial<AboutScreenStateProps>,
    Partial<AppStyleProps> {}

export interface AboutScreenStateProps {
  userName: string
  userAge: number
}
