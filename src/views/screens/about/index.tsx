import React from 'react'
import { Text, View } from 'react-native'
import { AppStore } from '../../../store'
import { AppStyle } from '../../styles'
import { AboutScreenProps, AboutScreenStateProps } from './props'

@AppStore.withStoreClass<AboutScreenStateProps>(state => ({
  userName: state.user.name,
  userAge: state.user.age
}))
@AppStyle.withThemeClass()
export class AboutScreen extends React.Component<AboutScreenProps> {
  constructor(props: AboutScreenProps) {
    super(props)
  }

  public render() {
    const {
      userName,
      userAge,
      theme: { styles }
    } = this.props as Required<AboutScreenProps>

    return (
      <View style={styles.container}>
        <Text>Name {userName}</Text>
        <Text>Age {userAge.toString()}</Text>
      </View>
    )
  }
}
