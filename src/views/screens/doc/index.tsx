import React from 'react'
import { Text, View } from 'react-native'
import { AppStore } from '../../../store'
import { setUserNameAction } from '../../../store/actions/set-user-name-action'
import { AppStyle } from '../../styles'
import {
  DocScreenActionProps,
  DocScreenProps,
  DocScreenStateProps
} from './props'

@AppStyle.withThemeClass()
@AppStore.withStoreClass<DocScreenStateProps, DocScreenActionProps>(
  state => ({ userAge: state.user.age, userName: state.user.name }),
  dispatch => ({ setUserName: payload => dispatch(setUserNameAction(payload)) })
)
export class DocScreen extends React.Component<DocScreenProps> {
  constructor(props: DocScreenProps) {
    super(props)
  }

  public render() {
    return (
      <View>
        <Text>DocScreen</Text>
      </View>
    )
  }
}
