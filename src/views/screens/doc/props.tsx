import { AppStyleProps } from '../../styles'

export interface DocScreenProps
  extends Partial<AppStyleProps>,
    Partial<DocScreenStateProps>,
    Partial<DocScreenActionProps> {}

export interface DocScreenStateProps {
  userName: string
  userAge: number
}

export interface DocScreenActionProps {
  setUserName: (payload: string) => void
}
