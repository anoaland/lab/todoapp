import React from 'react'
import { Alert, Image, Platform, StyleSheet, Text, View } from 'react-native'
import { AppData } from '../../../data'
import { SmallButton } from '../../components/small-button'
import { AppStyle } from '../../styles'
import { MainScreenProps } from './props'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu'
})

@AppStyle.withThemeClass()
export class MainScreen extends React.Component<MainScreenProps> {
  constructor(props: MainScreenProps) {
    super(props)
  }

  public render() {
    const { theme } = this.props as Required<MainScreenProps>

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Hello {this.props.foo}!</Text>
        <Image
          source={require('../../../../assets/logo.png')}
          style={{ marginVertical: 20, width: 185, height: 185 }}
          resizeMethod="scale"
        />
        <Text style={styles.instructions}>To get started, edit:</Text>
        <Text style={styles.instructions}>
          ./src/views/sreens/main/index.tsx
        </Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <SmallButton
          text="Just Test"
          onPress={() => {
            Alert.alert('Pressed!')
          }}
        />
        <SmallButton
          text="Change To Default Theme"
          onPress={() => {
            theme.change('default')
            AppData.App.setTheme('default')
          }}
        />
        <SmallButton
          text="Change To Red Theme"
          onPress={() => {
            theme.change('red')
            AppData.App.setTheme('red')
          }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
})
