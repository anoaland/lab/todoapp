import { createBottomTabNavigator } from 'react-navigation'
import { AboutScreen } from '../about'
import { HelpScreen } from '../help'
import { MainScreen } from '../main'

export const RootScreenNav = createBottomTabNavigator({
  About: AboutScreen,
  Help: HelpScreen,
  Main: MainScreen
})
