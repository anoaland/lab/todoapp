import { Column, Primary } from 'sqlite-ts'

export class App {
  @Primary()
  id?: number = 0

  @Column('NVARCHAR')
  theme: string = ''
}
