import { Db } from 'sqlite-ts'
import { Models } from '..'

type ThemeNames = 'default' | 'red'

export class App {
  db: Db<Models>
  constructor(db: Db<Models>) {
    this.db = db
  }

  async setTheme(theme: ThemeNames) {
    await this.db.tables.App.upsert({ id: 1, theme })
  }

  async getTheme(): Promise<ThemeNames> {
    const app = await this.db.tables.App.single().where(c =>
      c.equals({ id: 1 })
    )

    if (!app) {
      return 'default'
    }

    return app.theme as ThemeNames
  }

  // TODO: add provider functions...
}
