import { AppThunkAction } from '..'

export function setUserNameAction(payload: string): AppThunkAction {
  return dispatch => {
    dispatch({ type: 'USER/NAME', payload })
  }
}
