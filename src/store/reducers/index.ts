// tslint:disable:ordered-imports

// --------------------------------------------------------- //
// Please do not edit this file. This file is autogenerated. //
// --------------------------------------------------------- //

import { combineReducers } from 'redux'
import UserReducer from './user'
import UserActions from './user/actions'

export const reducers = combineReducers({
  user: UserReducer
})

export type AppRootActions = UserActions

export type AppRootState = ReturnType<typeof reducers>
