import { Reducer } from 'redux'
import UserActions from './actions'
import UserState from './state'

const UserReducer: Reducer<UserState, UserActions> = (
  state = {
    name: '',
    age: 0
  },
  action
) => {
  switch (action.type) {
    case 'USER/NAME':
      return { ...state, name: action.payload }

    case 'USER/AGE':
      return { ...state, age: action.payload }

    default:
      return state
  }
}

export default UserReducer
