type UserActions =
  | {
      type: 'USER/NAME'
      payload: string
    }
  | {
      type: 'USER/AGE'
      payload: number
    }

export default UserActions
